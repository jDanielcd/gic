import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoadPage } from "../pages/load/load";
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from "../pages/register/register";
import { ResetPasswordPage } from "../pages/reset-password/reset-password";
import { MyCompanyPage } from '../pages/my-company/my-company';
import { ConceptsPage } from '../pages/concepts/concepts';
import { ConceptDetailPage } from '../pages/concept-detail/concept-detail';
import { CompaniesPage } from '../pages/companies/companies';
import { CompanyDetailPage } from '../pages/company-detail/company-detail';
import { InvoicesPage } from "../pages/invoices/invoices";
import { InvoiceDetailPage } from "../pages/invoice-detail/invoice-detail";
import { ModalSearchCustomersPage } from "../pages/modal-search-customers/modal-search-customers";
import { ModalAddCustomerPage } from "../pages/modal-add-customer/modal-add-customer";
import { ModalSearchConceptPage } from "../pages/modal-search-concept/modal-search-concept";
import { ModalAddConceptPage } from "../pages/modal-add-concept/modal-add-concept";
import { PaymentsPage } from "../pages/payments/payments";
import { PaymentDetailPage } from "../pages/payment-detail/payment-detail";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthProvider } from '../providers/auth/auth';
import { MyCompanyDbProvider } from '../providers/my-company-db/my-company-db';
import { ConceptsDbProvider } from '../providers/concepts-db/concepts-db';
import { TaxesProvider } from '../providers/taxes/taxes';
import { CompaniesDbProvider } from '../providers/companies-db/companies-db';
import { InvoicesDbProvider } from '../providers/invoices-db/invoices-db';
import { TaxWithholdingsProvider } from '../providers/tax-withholdings/tax-withholdings';
import {DatePipe, DecimalPipe} from "@angular/common";
import { PaymentsDbProvider } from '../providers/payments-db/payments-db';

import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

export const firebaseConfig = {
  apiKey: "AIzaSyA49FKM3x_uWHk_6dKF6xI6S_LLRkwsPmI",
  authDomain: "tfgrado-d4030.firebaseapp.com",
  databaseURL: "https://tfgrado-d4030.firebaseio.com",
  storageBucket: "tfgrado-d4030.appspot.com",
  messagingSenderId: "10268029644"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoadPage,
    LoginPage,
    RegisterPage,
    ResetPasswordPage,
    MyCompanyPage,
    ConceptsPage,
    ConceptDetailPage,
    CompaniesPage,
    CompanyDetailPage,
    InvoicesPage,
    InvoiceDetailPage,
    ModalSearchCustomersPage,
    ModalAddCustomerPage,
    ModalSearchConceptPage,
    ModalAddConceptPage,
    PaymentsPage,
    PaymentDetailPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoadPage,
    LoginPage,
    RegisterPage,
    ResetPasswordPage,
    MyCompanyPage,
    ConceptsPage,
    ConceptDetailPage,
    CompaniesPage,
    CompanyDetailPage,
    InvoicesPage,
    InvoiceDetailPage,
    ModalSearchCustomersPage,
    ModalAddCustomerPage,
    ModalSearchConceptPage,
    ModalAddConceptPage,
    PaymentsPage,
    PaymentDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    MyCompanyDbProvider,
    ConceptsDbProvider,
    TaxesProvider,
    CompaniesDbProvider,
    InvoicesDbProvider,
    TaxWithholdingsProvider,
    DecimalPipe,
    DatePipe,
    PaymentsDbProvider,
    File,
    FileOpener
  ]
})
export class AppModule {}
