import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LoginPage } from "../pages/login/login";
import { MyCompanyPage } from '../pages/my-company/my-company';

import { AuthProvider } from '../providers/auth/auth';
import {ConceptsPage} from "../pages/concepts/concepts";
import {CompaniesPage} from "../pages/companies/companies";
import {InvoicesPage} from "../pages/invoices/invoices";
import {InvoiceDetailPage} from "../pages/invoice-detail/invoice-detail";
import {PaymentsPage} from "../pages/payments/payments";
import {LoadPage} from "../pages/load/load";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoadPage;

  pages: Array<{title: string, icon: string, component: any}>;

  constructor (public platform: Platform,
               public statusBar: StatusBar,
               public splashScreen: SplashScreen,
               private auth: AuthProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', icon: 'home', component: HomePage},
      { title: 'Facturas', icon: 'filing', component: InvoicesPage },
      { title: 'Compañías', icon: 'briefcase', component: CompaniesPage },
      { title: 'Artículos y servicios', icon: 'cube', component: ConceptsPage },
      { title: 'Pagos', icon: 'cash', component: PaymentsPage },
      { title: 'Mi compañía', icon: 'contact', component: MyCompanyPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.auth.Session.subscribe(session=>{
        if(session){
          this.rootPage = HomePage;
        }
        else{
          this.rootPage = LoginPage;
        }
      });

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  logOut() {
    console.log("SALIR");
    this.auth.logout();
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
