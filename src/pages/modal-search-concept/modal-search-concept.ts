import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import * as _ from 'lodash';

@IonicPage()
@Component({
  selector: 'page-modal-search-concept',
  templateUrl: 'modal-search-concept.html',
})
export class ModalSearchConceptPage {

  concepts: any;
  filterConcepts; any;
  searchConcept = '';

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public viewCtrl: ViewController) {
    this.concepts = this.navParams.data.concepts;
    this.filterConcepts = this.concepts;
  }

  ionViewDidLoad() {}

  closeModal() {
    this.viewCtrl.dismiss(null);
  }

  selectConcept(concept) {
    this.viewCtrl.dismiss(concept);
  }

  onSearch() {
    if (this.searchConcept.length > 0) {
      this.filterConcepts =  _.filter(this.concepts, (c) => {
        return _.kebabCase(c.name).search(_.kebabCase(this.searchConcept)) > -1
      });

    } else {
      this.filterConcepts = this.concepts;
    }
  }

}
