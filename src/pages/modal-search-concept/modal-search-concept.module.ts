import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalSearchConceptPage } from './modal-search-concept';

@NgModule({
  declarations: [
    ModalSearchConceptPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalSearchConceptPage),
  ],
})
export class ModalSearchConceptPageModule {}
