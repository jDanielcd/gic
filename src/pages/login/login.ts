import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import {ResetPasswordPage} from "../reset-password/reset-password";
import {RegisterPage} from "../register/register";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = { email: '', password: '' };

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public auth: AuthProvider,
               public alertCtrl: AlertController)
  {}

  ionViewDidLoad()
  {
    console.log('ionViewDidLoad LoginPage');
  }

  signin() {
    this.navCtrl.push(RegisterPage);
  }

  login()
  {
    this.auth.loginUser(this.user.email,this.user.password ).then((user) => {
      }
    )
      .catch(err=>{
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Los datos introducidos no son correctos. Puede volver a intentarlo.',
          buttons: ['Aceptar']
        });
        alert.present();
      })
  }

  resetPassword() {
    this.navCtrl.push(ResetPasswordPage);
  }


}
