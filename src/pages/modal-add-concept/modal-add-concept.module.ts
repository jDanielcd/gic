import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAddConceptPage } from './modal-add-concept';

@NgModule({
  declarations: [
    ModalAddConceptPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalAddConceptPage),
  ],
})
export class ModalAddConceptPageModule {}
