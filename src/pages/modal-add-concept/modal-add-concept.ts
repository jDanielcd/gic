import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {TaxesProvider} from "../../providers/taxes/taxes";

/**
 * Generated class for the ModalAddConceptPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-add-concept',
  templateUrl: 'modal-add-concept.html',
})
export class ModalAddConceptPage {

  concept = {name: '', description: '', price: 0, tax: ''};
  taxes: any;

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public viewCtrl: ViewController,
               public taxesProvider: TaxesProvider) {
    this.taxes = this.taxesProvider.getTaxes();
  }

  ionViewDidLoad() {}

  closeModal() {
    this.viewCtrl.dismiss(null);
  }

  addConcept() {
    this.viewCtrl.dismiss(this.concept);
  }

  get conceptIsComplete() {
    if (this.concept.name.length > 0 &&
      this.concept.price > 0) {
      return true;

    } else {
      return false;
    }
  }

}
