import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  user = { email: '', password: '' };

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public auth: AuthProvider,
               public alertCtrl: AlertController)
  {
  }

  ionViewDidLoad() {}

  signin()
  {
    this.auth.registerUser(this.user.email,this.user.password)
      .then((user) => {
        // El usuario se ha creado correctamento
      })
      .catch(err=>{
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Debe introducir una cuenta de correo electrónico correcto.',
          buttons: ['Aceptar']
        });
        alert.present();
      })
  }

}
