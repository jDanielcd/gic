import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConceptDetailPage } from './concept-detail';

@NgModule({
  declarations: [
    ConceptDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ConceptDetailPage),
  ],
})
export class ConceptDetailPageModule {}
