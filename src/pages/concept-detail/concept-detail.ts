import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {TaxesProvider} from "../../providers/taxes/taxes";
import {ConceptsDbProvider} from "../../providers/concepts-db/concepts-db";

/**
 * Generated class for the ConceptDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-concept-detail',
  templateUrl: 'concept-detail.html',
})
export class ConceptDetailPage {

  newConcept: any;
  taxes: any;
  loading: boolean = true;

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public toastCtrl: ToastController,
               public taxesProvider: TaxesProvider,
               public conceptsProvider: ConceptsDbProvider)
  {
    this.taxes = this.taxesProvider.getTaxes();

    this.conceptsProvider.getConcept(this.navParams.get('id')).subscribe(concept => {
      this.newConcept = concept;
      if (this.newConcept === null) {
        this.newConcept = {id: '', name: '', description: '', tax: '', price: '' };
      }
      this.loading = false;
    });
  }

  ionViewDidLoad() {
  }

  addConcept()
  {
    this.conceptsProvider.addConcept(this.newConcept);
    this.navCtrl.pop();
  }

  updateConcept()
  {
    this.conceptsProvider.updateConcept(this.newConcept);
    this.navCtrl.pop();
  }

  get conceptIsComplete() {
    if (this.newConcept.name.length > 0 &&
        this.newConcept.price > 0) {
      return true;

    } else {
      return false;
    }
  }

}
