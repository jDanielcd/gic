import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import {InvoiceDetailPage} from "../invoice-detail/invoice-detail";
import {InvoicesDbProvider} from "../../providers/invoices-db/invoices-db";
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import {TaxesProvider} from "../../providers/taxes/taxes";
import {DatePipe, DecimalPipe} from "@angular/common";
import {TaxWithholdingsProvider} from "../../providers/tax-withholdings/tax-withholdings";
import {MyCompanyDbProvider} from "../../providers/my-company-db/my-company-db";
import * as _ from "lodash";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@IonicPage()
@Component({
  selector: 'page-invoices',
  templateUrl: 'invoices.html',
})
export class InvoicesPage {

  invoices: any;
  filterInvoices: any;
  searchInvoice: '';
  loading: boolean = true;
  pdfObj = null;
  supplier: any;
  showFilters: boolean = false;
  filterState = 'todas';

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public invoicesProvider: InvoicesDbProvider,
               public taxesProvider: TaxesProvider,
               public withholdingsProvider: TaxWithholdingsProvider,
               public myCompanyProvider: MyCompanyDbProvider,
               private plt: Platform,
               private file: File,
               private fileOpener: FileOpener,
               private decimalPipe: DecimalPipe,
               private datePipe: DatePipe) {
  }

  ionViewDidLoad() {
    this.invoicesProvider.getInvoices().subscribe(invoices => {
      this.invoices = invoices;
      this.filterInvoices = this.invoices;
      this.filterInvoices = _.orderBy(this.invoices, ['date'], ['desc']);
      this.loading = false;
    });

    this.myCompanyProvider.getMyCompany().subscribe(mycompany => {
      this.supplier = mycompany;
    })
  }

  changeState(invoice, newState) {
    invoice.state = newState;
    this.invoicesProvider.updateInvoice(invoice);
  }

  addInvoice() {
    this.navCtrl.push(InvoiceDetailPage, { id: 0 });
  }

  updateInvoice(invoice) {
    this.navCtrl.push(InvoiceDetailPage, { id: invoice.id });
  }

  deleteInvoice(invoice) {
    this.invoicesProvider.deleteInvoice(invoice.id);
  }

  onSearch() {
    if (this.searchInvoice.length > 0) {
      this.filterInvoices =  _.filter(this.invoices, (i) => {
        return _.kebabCase(i.state).search(_.kebabCase(this.filterState)) > -1
      });
      this.filterInvoices =  _.filter(this.filterInvoices, (i) => {
        return _.kebabCase(i.customerName + i.invoice + i.state).search(_.kebabCase(this.searchInvoice)) > -1
      });

    } else {
      this.filterInvoices =  _.filter(this.invoices, (i) => {
        return _.kebabCase(i.state).search(_.kebabCase(this.filterState)) > -1
      });
    }
    this.filterInvoices = _.orderBy(this.filterInvoices, ['date'], ['desc']);
  }

  onRadioSearch(type) {
    this.filterState = type;
    this.filterInvoices =  _.filter(this.invoices, (i) => {
      return _.kebabCase(i.state).search(_.kebabCase(this.filterState)) > -1
    });
    this.filterInvoices = _.orderBy(this.filterInvoices, ['date'], ['desc']);
  }

  downloadPDF(invoice) {
    let invoicePDF = {
      content: [
        {
          style: 'conceptsTable',
          table: {
            widths: ['*', '*'],
            headerRows: 1,
            body: [
              [{text: 'FACTURA ' + invoice.invoice, alignment: 'left', style: 'headerInvoiceTable'},
                {text: this.datePipe.transform(invoice.date, 'dd/MM/yyyy'), alignment: 'right', style: 'headerInvoiceTable'}]
            ]
          },
          layout: 'noBorders'
        },
        {
          style: 'conceptsTable',
          table: {
            widths: ['*', '*'],
            headerRows: 1,
            body: [
              [{text: 'CLIENTE', alignment: 'center', style: 'headerConceptsTable'}, {text: 'PROVEEDOR', alignment: 'center', style: 'headerConceptsTable'}],
              [
                {text: this.generateInfoCustomer(invoice), alignment: 'center', style: 'lineConceptsTable'},
                {text: this.generateInfoSupplier(), alignment: 'center', style: 'lineConceptsTable'}
                ]
            ]
          },
          layout: 'noBorders'
        },
        {
          style: 'conceptsTable',
          table: {
            widths: ['*', '*', '*', '*', '*', '*'],
            headerRows: 1,
            body: this.generateConceptsTable(invoice.concepts)
          },
          layout: 'noBorders'
        },
        {
          style: 'conceptsTable',
          table: {
            widths: ['*', '*', '*', '*'],
            headerRows: 1,
            body: this.generateTotalsTable(invoice)
          },
          layout: 'noBorders'
        },
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          color: '#2D9C6A',
        },
        headerInvoiceTable: {
          fontSize: 18,
          bold: true,
          color: '#2D9C6A'
        },
        headerConceptsTable: {
          fontSize: 12,
          bold: true,
          color: '#2D9C6A',
          margin: [0, 30, 0, 0]
        },
        lineConceptsTable: {
          fontSize: 10,
          margin: [0, 10, 0, 0]
        },
        headerTotalsTable: {
          fontSize: 12,
          bold: true,
          color: '#2D9C6A',
          margin: [0, 30, 0, 0]
        },
      },
      defaultStyle: {}
    }

    this.pdfObj = pdfMake.createPdf(invoicePDF);

    if (this.plt.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        let blob = new Blob([buffer], { type: 'application/pdf' });

        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
      this.pdfObj = null;
    }
  }

  generateInfoCustomer(invoice) {
    let customer = invoice.customer;
    return customer.name + '\n' + customer.address + '\n' +
    customer.city + ' - ' + customer.postalCode + '\n' + customer.province + '\n' +
    customer.phone + '\n' + customer.email;
  }

  generateInfoSupplier() {
    return this.supplier.name + '\n' + this.supplier.address + '\n' +
      this.supplier.city + ' - ' + this.supplier.postalCode + '\n' + this.supplier.province + '\n' +
      this.supplier.phone + '\n' + this.supplier.email;
  }

  generateConceptsTable(concepts) {
    let taxes = this.taxesProvider.getTaxes();
    let table = [
      [{text: 'Concepto', alignment: 'center', style: 'headerConceptsTable'},
        {text: 'Cantidad', alignment: 'center', style: 'headerConceptsTable'},
        {text: 'Precio', alignment: 'center', style: 'headerConceptsTable'},
        {text: 'Impuesto', alignment: 'center', style: 'headerConceptsTable'},
        {text: 'Descuento', alignment: 'center', style: 'headerConceptsTable'},
        {text: 'Subtotal', alignment: 'center', style: 'headerConceptsTable'}]
    ];

    concepts.forEach(function(c) {
      let conceptTax = taxes[c.tax-1].name + ' (' + taxes[c.tax-1].value.toFixed(2) + ' %)';
      let conceptSubtotal = c.quantity*c.price;
      conceptSubtotal += (conceptSubtotal*taxes[c.tax-1].value)/100;
      conceptSubtotal -= (conceptSubtotal*c.discount)/100;
      let conceptLine = [
        {text: c.concept.name, alignment: 'center', style: 'lineConceptsTable'},
        {text: c.quantity, alignment: 'center', style: 'lineConceptsTable'},
        {text: c.price + ' €', alignment: 'center', style: 'lineConceptsTable'},
        {text: conceptTax, alignment: 'center', style: 'lineConceptsTable'},
        {text: c.discount.toFixed(2) + ' %', alignment: 'center', style: 'lineConceptsTable'},
        {text: conceptSubtotal.toFixed(2) + ' €', alignment: 'center', style: 'lineConceptsTable'}
        ];
      table.push(conceptLine);
    });

    return table;
  }

  generateTotalsTable(invoice) {
    let taxes = this.taxesProvider.getTaxes();
    let taxWith = this.withholdingsProvider.getTaxesWithholdings();

    let subtotal = 0;
    let taxesSubtotal = 0;
    let withholdingsSubtotal = 0;
    let total = 0;

    let table = [
      [{text: 'Subtotal', alignment: 'center', style: 'headerTotalsTable'},
        {text: 'Impuestos', alignment: 'center', style: 'headerTotalsTable'},
        {text: 'Retenciones', alignment: 'center', style: 'headerTotalsTable'},
        {text: 'Total', alignment: 'center', style: 'headerTotalsTable'}]
    ];

    invoice.concepts.forEach(function(c) {
      let conceptTotal = c.quantity * c.price;
      conceptTotal -= (conceptTotal*c.discount)/100;
      taxesSubtotal += (conceptTotal*taxes[c.tax-1].value)/100;
      conceptTotal += (conceptTotal*taxes[c.tax-1].value)/100;
      subtotal += conceptTotal;
    });

    withholdingsSubtotal += (taxWith[invoice.withholding-1].value*subtotal)/100;
    total += subtotal - withholdingsSubtotal;

    let tableTotals = [
      {text: subtotal.toFixed(2) + ' €', alignment: 'center', style: 'lineConceptsTable'},
      {text: taxesSubtotal.toFixed(2) + ' €', alignment: 'center', style: 'lineConceptsTable'},
      {text: withholdingsSubtotal.toFixed(2) + ' €', alignment: 'center', style: 'lineConceptsTable'},
      {text: total.toFixed(2) + ' €', alignment: 'center', style: 'lineConceptsTable'}
    ];

    table.push(tableTotals);

    return table;
  }

}
