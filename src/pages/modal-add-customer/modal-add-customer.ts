import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the ModalAddCustomerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-add-customer',
  templateUrl: 'modal-add-customer.html',
})
export class ModalAddCustomerPage {

  customer = {id: 0, type: 1, name: '', nif: '', address: '', city: '', province: '', postalCode: '', phone: '', email: ''};

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
  }

  closeModal() {
    this.viewCtrl.dismiss(null);
  }

  addCustomer() {
    this.viewCtrl.dismiss(this.customer);
  }

  get companyIsComplete() {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let validateEmail =  re.test(String(this.customer.email).toLowerCase());

    if (this.customer.name.length > 0 &&
      this.customer.nif.length > 0 &&
      this.customer.phone.length > 0 &&
      this.customer.email.length > 0 &&
      validateEmail &&
      this.customer.address.length > 0 &&
      this.customer.postalCode.length > 0 &&
      this.customer.city.length > 0 &&
      this.customer.province.length > 0 ) {
      return true;

    } else {
      return false;
    }
  }

}
