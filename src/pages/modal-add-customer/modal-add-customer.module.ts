import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAddCustomerPage } from './modal-add-customer';

@NgModule({
  declarations: [
    ModalAddCustomerPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalAddCustomerPage),
  ],
})
export class ModalAddCustomerPageModule {}
