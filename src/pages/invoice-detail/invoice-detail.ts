import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, ViewController} from 'ionic-angular';
import {CompaniesDbProvider} from "../../providers/companies-db/companies-db";
import * as _ from 'lodash';
import {ConceptsDbProvider} from "../../providers/concepts-db/concepts-db";
import {TaxWithholdingsProvider} from "../../providers/tax-withholdings/tax-withholdings";
import {CompaniesPage} from "../companies/companies";
import {ModalSearchCustomersPage} from "../modal-search-customers/modal-search-customers";
import {ModalAddCustomerPage} from "../modal-add-customer/modal-add-customer";
import {ModalAddConceptPage} from "../modal-add-concept/modal-add-concept";
import {ModalSearchConceptPage} from "../modal-search-concept/modal-search-concept";
import {TaxesProvider} from "../../providers/taxes/taxes";
import {DecimalPipe} from "@angular/common";
import {InvoicesDbProvider} from "../../providers/invoices-db/invoices-db";

@IonicPage()
@Component({
  selector: 'page-invoice-detail',
  templateUrl: 'invoice-detail.html',
})
export class InvoiceDetailPage {

  invoice: any;
  invoices: any;
  loadingInvoices: boolean = true;
  id = 0;
  customers: any;
  filterCustomers: any;
  selectedCustomer: any;
  concepts: any;
  taxes= [];
  withholdings: any;
  loading: boolean = true;
  loadingCustomers: boolean = true;
  loadingConcepts: boolean = true;

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public modalCtrl: ModalController,
               public viewCtrl: ViewController,
               public companiesProvider: CompaniesDbProvider,
               public conceptsProvider: ConceptsDbProvider,
               public taxesProvider: TaxesProvider,
               public withholdingsProvider: TaxWithholdingsProvider,
               public invoiceProvider: InvoicesDbProvider,
               public decimalPipe: DecimalPipe)
  {
    this.id = this.navParams.get('id');

    if (this.navParams.get('id') != 0) {
      this.loading = true;
      this.invoiceProvider.getInvoice(this.navParams.get('id')).subscribe( invoice => {
        this.invoice = invoice;
        this.selectedCustomer = this.invoice.customer;
        this.loading = false;
      });

    } else {
      this.invoice = {
        customerName:'',
        invoice: '',
        date: '',
        withholding: null,
        concepts: [],
        total: 0
      };
      this.loading = false;
    }

    this.companiesProvider.getCompanies().subscribe(companies => {
      this.customers = companies;
      this.customers = _.filter(this.customers, function(o) { return o.type == "1"; });

      this.customers.forEach(function (customer) {
        let initials = customer.name.match(/\b\w/g) || [];
        initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
        customer.test = initials;
      });

      this.filterCustomers = this.customers;
      this.loadingCustomers = false;
    });

    this.conceptsProvider.getConcepts().subscribe( concepts => {
      this.concepts = concepts;
      this.loadingConcepts = false;
    });

    this.invoiceProvider.getInvoices().subscribe( invoices => {
      this.invoices = invoices;
      this.loadingInvoices = false;
    });

    this.taxes = this.taxesProvider.getTaxes();
    this.withholdings = this.withholdingsProvider.getTaxesWithholdings();
  }

  ionViewDidLoad() {}

  addCustomerModal() {
    let addCustomerModal = this.modalCtrl.create(ModalAddCustomerPage);
    addCustomerModal.onDidDismiss( data => {
      if (data != null) {
        this.selectedCustomer = data;
        this.invoice.customerName = data.name;
      }
    });
    addCustomerModal.present();
  }

  searchCustomersModal() {
    let searchCustomersModal = this.modalCtrl.create(ModalSearchCustomersPage, { customers: this.customers });
    searchCustomersModal.onDidDismiss( data => {
      if (data != null) {
        this.selectedCustomer = data;
        this.invoice.customerName = data.name;
      }
    });
    searchCustomersModal.present();
  }

  addConceptModal() {
    let addConceptModal = this.modalCtrl.create(ModalAddConceptPage);
    addConceptModal.onDidDismiss( data => {
      if (data != null) {
        let newConcept = { concept: data, quantity: this.decimalPipe.transform(0, '1.2-2'), price: this.decimalPipe.transform(data.price, '1.2-2'), tax: data.tax, discount: 0, total: 0 };
        this.invoice.concepts.push(newConcept);
      }
    });
    addConceptModal.present();
  }

  searchConceptsModal() {
    let searchConceptsModal = this.modalCtrl.create(ModalSearchConceptPage, { concepts: this.concepts });
    searchConceptsModal.onDidDismiss( data => {
      if (data != null) {
        let newConcept = { concept: data, quantity: this.decimalPipe.transform(0, '1.2-2'), price: this.decimalPipe.transform(data.price, '1.2-2'), tax: data.tax, discount: 0, total: 0 };
        this.invoice.concepts.push(newConcept);
      }
    });
    searchConceptsModal.present();
  }

  calculateTotal() {
    let total = 0;
    let taxes = this.taxes;
    this.invoice.concepts.forEach(function(concept) {
      let conceptTotal = concept.quantity * concept.price;
      conceptTotal += (conceptTotal*taxes[concept.concept.tax - 1].value)/100;
      conceptTotal -= (conceptTotal*concept.discount)/100;
      total += conceptTotal;
    });
    if (this.invoice.withholding != null) {
      let taxWithholding = this.withholdings[this.invoice.withholding-1].value;
      total -= (total*taxWithholding)/100;
    }
    this.invoice.total = this.decimalPipe.transform(total, '1.2-2');
  }

  clearCustomer() { this.selectedCustomer = null; }

  addInvoice() {
    this.invoice.customer = this.selectedCustomer;
    this.invoice.state = 'pendiente';
    this.invoice.created = Date.now();
    this.invoiceProvider.addInvoice(this.invoice);
    this.navCtrl.pop();
  }

  updateInvoice() {
    this.invoiceProvider.updateInvoice(this.invoice);
    this.navCtrl.pop();
  }

  get searchNumberOfInvoice() {
    let state = false;
    if (this.id == 0) {
      let idToSearch = this.invoice.invoice;

      this.invoices.forEach(function(element) {
        if (element.invoice == idToSearch) {
          state = true;
        }
      });
    }

    return state;
  }

  get invoiceIsComplete() {
    if (!this.loadingInvoices &&
        !this.searchNumberOfInvoice &&
        this.invoice.invoice.length > 0 &&
        this.invoice.customerName.length > 0 &&
        this.invoice.date.length > 0 &&
        this.invoice.withholding != null &&
        this.invoice.concepts.length > 0) {
      return true;

    } else {
      return false;
    }
  }

}
