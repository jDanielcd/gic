import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PaymentDetailPage} from "../payment-detail/payment-detail";
import {PaymentsDbProvider} from "../../providers/payments-db/payments-db";
import * as _ from "lodash";

@IonicPage()
@Component({
  selector: 'page-payments',
  templateUrl: 'payments.html',
})
export class PaymentsPage {

  payments: any;
  filterPayments: any;
  searchPayment: '';
  loading: boolean = true;

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public paymentsProvider: PaymentsDbProvider)
  {
  }

  ionViewDidLoad() {
    this.paymentsProvider.getPayments().subscribe(payments => {
      this.payments = payments;
      this.filterPayments = this.payments;
      this.loading = false;
    });
  }

  addPayment() {
    this.navCtrl.push(PaymentDetailPage, { id: 0 });
  }

  updatePayment(payment) {
    this.navCtrl.push(PaymentDetailPage, { id: payment.id });
  }

  deletePayment(payment) {
    this.paymentsProvider.deletePayment(payment);
  }

  onSearch($envent) {
    if (this.searchPayment.length > 0) {
      this.filterPayments =  _.filter(this.payments, (c) => {
        return _.kebabCase(c.name+c.supplierData.name).search(_.kebabCase(this.searchPayment)) > -1
      });

    } else {
      this.filterPayments = this.payments;
    }
  }

}
