import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalSearchCustomersPage } from './modal-search-customers';

@NgModule({
  declarations: [
    ModalSearchCustomersPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalSearchCustomersPage),
  ],
})
export class ModalSearchCustomersPageModule {}
