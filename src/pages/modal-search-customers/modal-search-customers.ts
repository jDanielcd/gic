import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import * as _ from 'lodash';

@IonicPage()
@Component({
  selector: 'page-modal-search-customers',
  templateUrl: 'modal-search-customers.html',
})
export class ModalSearchCustomersPage {

  customers: any;
  filterCustomers; any;
  searchCustomer = '';

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public modalCtrl: ModalController,
               public viewCtrl: ViewController) {
    this.customers = this.navParams.data.customers;
    this.filterCustomers = this.customers;
  }

  closeModal() {
    this.viewCtrl.dismiss(null);
  }

  selectCustomer(customer) {
    this.viewCtrl.dismiss(customer);
    // this.navCtrl.pop();
  }

  onSearch() {
    if (this.searchCustomer.length > 0) {
      this.filterCustomers =  _.filter(this.customers, (c) => {
        return _.kebabCase(c.name).search(_.kebabCase(this.searchCustomer)) > -1
      });

    } else {
      this.filterCustomers = this.customers;
    }
  }

}
