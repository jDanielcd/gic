import {Component, ViewChild} from '@angular/core';
import {IonicPage, Nav, NavController, NavParams} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {LoginPage} from "../login/login";

/**
 * Generated class for the ResetPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {

  email: '';

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public authProvider: AuthProvider)
  {
  }

  ionViewDidLoad() {}

  resetPwd() {
    this.authProvider.resetPassword(this.email).then( authService => {
      this.navCtrl.popToRoot();
    });
  }


}
