import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {CompanyDetailPage} from "../company-detail/company-detail";
import {CompaniesDbProvider} from "../../providers/companies-db/companies-db";
import {ConceptDetailPage} from "../concept-detail/concept-detail";
import * as _ from 'lodash';

/**
 * Generated class for the CompaniesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-companies',
  templateUrl: 'companies.html',
})
export class CompaniesPage {

  companies: any;
  loading: boolean = true;
  searchCompany = '';

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              public companiesProvider: CompaniesDbProvider) {
  }

  ionViewDidLoad() {
    this.companiesProvider.getCompanies().subscribe(companies => {
      this.companies = companies;
      this.loading = false;
    });
  }

  addCompany() {
    this.navCtrl.push(CompanyDetailPage, { id: 0 });
  }

  updateCompany (company) {
    this.navCtrl.push(CompanyDetailPage, { id: company.id });
  }

  deleteCompany (company) {
    this.companiesProvider.deleteCompany(company);
    const toast = this.toastCtrl.create({
      message: 'Se ha eliminado la compañía',
      duration: 3000
    });
    toast.present();
  }

  onSearch($envent) {
    if (this.searchCompany.length > 0) {
      this.companies =  _.filter(this.companies, (c) => {
        return _.kebabCase(c.name).search(_.kebabCase(this.searchCompany)) > -1
      });

    } else {
      this.companiesProvider.getCompanies().subscribe(companies => {
        this.companies = companies;
      });
    }
  }

  onCancelSearch($envent) {
    this.companiesProvider.getCompanies().subscribe(companies => {
      this.companies = companies;
    });
  }
}
