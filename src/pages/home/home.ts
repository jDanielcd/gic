import { Component, ViewChild } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import {InvoicesDbProvider} from "../../providers/invoices-db/invoices-db";
import {PaymentsDbProvider} from "../../providers/payments-db/payments-db";
import * as _ from 'lodash';
import { Chart } from 'chart.js';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  invoices: any;
  invoicesLength = 0;
  pendingInvoices = 0;
  acceptedInvoices = 0;
  rejectedInvoices = 0;
  payments: any;
  paymentsLength = 0;
  loading: boolean = true;

  @ViewChild('doughnutCanvas') doughnutCanvas;
  doughnutChart: any;

  @ViewChild('donutInvoices') donutInvoices;
  donutInvoicesChart: any;

  constructor (public navCtrl: NavController,
               public auth: AuthProvider,
               public invoicesProvider: InvoicesDbProvider,
               public paymentsProvider: PaymentsDbProvider)
  {
    this.invoicesProvider.getInvoices().subscribe( invoices => {
      this.invoices = invoices;
      this.invoicesLength = this.invoices.length;

      this.paymentsProvider.getPayments().subscribe( payments => {
        this.payments = payments;
        this.paymentsLength = this.payments.length;
        this.loading = false;
        this.getTotalsInvoicesByState();
        this.getGraphics();
      });
    });
  }

  ionViewDidLoad() {
  }

  getGraphics() {
    this.donutInvoicesChart = new Chart(this.donutInvoices.nativeElement, {

      type: 'doughnut',
      data: {
        labels: ["Pendientes", "Aceptadas", "Rechazadas"],
        datasets: [{
          label: '# of Votes',
          data: [this.pendingInvoices, this.acceptedInvoices, this.rejectedInvoices],
          backgroundColor: [
            'rgba(255, 225, 74, 0.5)',
            'rgba(66, 231, 157, 0.5)',
            'rgba(255, 107, 87, 0.5)'
          ],
          hoverBackgroundColor: [
            "rgb(255, 225, 74)",
            "rgb(66, 231, 157)",
            "rgb(255, 107, 87)"
          ]
        }]
      }

    });

    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

      type: 'doughnut',
      data: {
        labels: ["Facturas", "Pagos"],
        datasets: [{
          label: '# of Votes',
          data: [this.invoicesLength, this.paymentsLength],
          backgroundColor: [
            'rgba(66, 231, 157, 0.5)',
            'rgba(255, 107, 87, 0.5)'
          ],
          hoverBackgroundColor: [
            "rgb(66, 231, 157)",
            "rgb(255, 107, 87)"
          ]
        }]
      }

    });
  }

  getTotalsInvoicesByState() {
    this.pendingInvoices =  _.filter(this.invoices, (i) => {
      return _.kebabCase(i.state).search(_.kebabCase('pendiente')) > -1
    }).length;

    this.acceptedInvoices =  _.filter(this.invoices, (i) => {
      return _.kebabCase(i.state).search(_.kebabCase('aceptada')) > -1
    }).length;

    this.rejectedInvoices =  _.filter(this.invoices, (i) => {
      return _.kebabCase(i.state).search(_.kebabCase('rechazada')) > -1
    }).length;
  }

}
