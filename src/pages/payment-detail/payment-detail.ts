import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PaymentsDbProvider} from "../../providers/payments-db/payments-db";
import * as _ from "lodash";
import {CompaniesDbProvider} from "../../providers/companies-db/companies-db";

/**
 * Generated class for the PaymentDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-detail',
  templateUrl: 'payment-detail.html',
})
export class PaymentDetailPage {

  id = 0;
  payment: any;
  validPayment: boolean;
  loading: boolean = true;
  suppliers: any;

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public paymentsProvider: PaymentsDbProvider,
               public companiesProvider: CompaniesDbProvider)
  {
    this.id = this.navParams.get('id');

    this.paymentsProvider.getPayment(this.navParams.get('id')).subscribe(payment => {
      this.payment = payment;
      if (this.payment === null) {
        this.payment = { supplier: '', name: '', description: '', date: '', total: 0 };
      }

      this.loading = false;
    });

    this.companiesProvider.getCompanies().subscribe(companies => {
      this.suppliers = companies;
      this.suppliers = _.filter(this.suppliers, function(o) { return o.type == "2" || o.type == "3"; });
    });
  }

  ionViewDidLoad() {}

  addPayment() {
    let supplierId = this.payment.supplier;
    let supplierData = '';
    this.suppliers.forEach(function(sup) {
      if (sup.id == supplierId) {
        supplierData = sup;
      }
    });
    this.payment.supplierData = supplierData;
    this.paymentsProvider.addPayment(this.payment);
    this.navCtrl.pop();
  }

  updatePayment() {
    let supplierId = this.payment.supplier;
    let supplierData = '';
    this.suppliers.forEach(function(sup) {
      if (sup.id == supplierId) {
        supplierData = sup;
      }
    });
    this.payment.supplierData = supplierData;
    this.paymentsProvider.updatePayment(this.payment);
    this.navCtrl.pop();
  }

  get paymentIsComplete(){
    if (this.payment.supplier != '' &&
        this.payment.name.length > 0 &&
        this.payment.date.length > 0 &&
        this.payment.total > 0) {
      return true;

    } else {
      return false;
    }
  }

}
