import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { MyCompanyDbProvider } from '../../providers/my-company-db/my-company-db';

/**
 * Generated class for the MyCompanyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-company',
  templateUrl: 'my-company.html',
})
export class MyCompanyPage {

  myCompany: any;
  cargando: boolean = true;

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public myCompanyDbProvider: MyCompanyDbProvider,
               public toastCtrl: ToastController)
  {}

  ionViewDidLoad() {
    this.myCompanyDbProvider.getMyCompany().subscribe(myCompany => {
      this.myCompany = myCompany;
      this.cargando = false;
      if (this.myCompany === null) {
        this.myCompany = {name: '', nif: '', phone: '', email: '', address: '', postalCode: '', city: '', province: ''};
      }
    });
  }

  updateMyCompany()
  {
    this.myCompanyDbProvider.updateMyCompany(this.myCompany);
    const toast = this.toastCtrl.create({
      message: 'Se han actualizado los datos de la compañía',
      duration: 3000
    });
    toast.present();
  }

}
