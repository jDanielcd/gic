import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {ConceptDetailPage} from "../concept-detail/concept-detail";
import {ConceptsDbProvider} from "../../providers/concepts-db/concepts-db";
import * as _ from 'lodash';

/**
 * Generated class for the ConceptsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-concepts',
  templateUrl: 'concepts.html',
})
export class ConceptsPage {

  concepts: any;
  loading: boolean = true;
  searchConcept: '';

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public toastCtrl: ToastController,
               public conceptsProvider: ConceptsDbProvider) {
  }

  ionViewDidLoad() {
    this.conceptsProvider.getConcepts().subscribe(concepts => {
      this.concepts = concepts;
      this.loading = false;
    });
  }

  addConcept()
  {
    this.navCtrl.push(ConceptDetailPage, { id: 0 });
  }

  updateConcept(concept)
  {
    this.navCtrl.push(ConceptDetailPage, { id: concept.id });
  }

  deleteConcept(concept)
  {
    this.conceptsProvider.deleteConcept(concept);
    const toast = this.toastCtrl.create({
      message: 'Se ha eliminado el artículo o servicio',
      duration: 3000
    });
    toast.present();
  }

  onSearch($envent) {
    if (this.searchConcept.length > 0) {
      this.concepts =  _.filter(this.concepts, (c) => {
        return _.kebabCase(c.name).search(_.kebabCase(this.searchConcept)) > -1
      });

    } else {
      this.conceptsProvider.getConcepts().subscribe(concepts => {
        this.concepts = concepts;
      });
    }
  }

  onCancelSearch($envent) {
    this.conceptsProvider.getConcepts().subscribe(concepts => {
      this.concepts = concepts;
    });
  }
}
