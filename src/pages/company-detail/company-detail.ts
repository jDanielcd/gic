import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {CompaniesDbProvider} from "../../providers/companies-db/companies-db";

/**
 * Generated class for the CompanyDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-company-detail',
  templateUrl: 'company-detail.html',
})
export class CompanyDetailPage {

  loading: boolean = true;
  company: any;

  constructor (public navCtrl: NavController,
               public navParams: NavParams,
               public toastCtrl: ToastController,
               public companiesProvider: CompaniesDbProvider) {
    console.log(this.navParams.get('id'));

    this.companiesProvider.getCompany(this.navParams.get('id')).subscribe(company => {
      this.company = company;
      if (this.company === null) {
        this.company = {id: 0, type: '', name: '', nif: '', phone: '', email: '', address: '', postalCode: '', city: '', province: ''};
      }
      this.loading = false;
    });
  }

  ionViewDidLoad() {
  }

  addCompany() {
    this.companiesProvider.addCompany(this.company);
    this.navCtrl.pop();
  }

  updateCompany() {
    this.companiesProvider.updateCompany(this.company);
    this.navCtrl.pop();
  }

  get companyIsComplete() {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let validateEmail =  re.test(String(this.company.email).toLowerCase());

    if (this.company.type.length > 0 &&
        this.company.name.length > 0 &&
        this.company.nif.length > 0 &&
        this.company.phone.length > 0 &&
        this.company.email.length > 0 &&
        validateEmail &&
        this.company.address.length > 0 &&
        this.company.postalCode.length > 0 &&
        this.company.city.length > 0 &&
        this.company.province.length > 0 ) {
      return true;

    } else {
      return false;
    }
  }

}
