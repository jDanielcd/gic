import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AuthProvider } from "../auth/auth";
import * as _ from 'lodash';

@Injectable()
export class CompaniesDbProvider {

  constructor (public afDB: AngularFireDatabase,
               public auth: AuthProvider)
  {}

  getCompanies()
  {
    return this.afDB.list(this.auth.getUser() + '/companies').valueChanges();
  }

  getCompany(id) {
    return this.afDB.object(this.auth.getUser() + '/companies/' + id).valueChanges();
  }

  addCompany(company) {
    company.id = Date.now();
    this.afDB.database.ref(this.auth.getUser() + '/companies/' + company.id).set(company);
  }

  updateCompany(company) {
    this.afDB.database.ref(this.auth.getUser() + '/companies/' + company.id).set(company);
  }

  deleteCompany(company) {
    this.afDB.database.ref(this.auth.getUser() + '/companies/' + company.id).remove();
  }

}
