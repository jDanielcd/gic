import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AuthProvider } from "../auth/auth";

@Injectable()
export class ConceptsDbProvider {

  constructor (public afDB: AngularFireDatabase,
               public auth: AuthProvider) {
  }

  getConcepts()
  {
    return this.afDB.list(this.auth.getUser() + '/concepts').valueChanges();
  }

  getConcept(id)
  {
    return this.afDB.object(this.auth.getUser() + '/concepts/' + id).valueChanges();
  }

  addConcept(concept)
  {
    concept.id  = Date.now();
    return this.afDB.database.ref(this.auth.getUser() + '/concepts/' + concept.id).set(concept);
  }

  updateConcept(concept)
  {
    this.afDB.database.ref(this.auth.getUser() + '/concepts/' + concept.id).set(concept);
  }

  deleteConcept(concept)
  {
    this.afDB.database.ref(this.auth.getUser() + '/concepts/' + concept.id).remove();
  }

}
