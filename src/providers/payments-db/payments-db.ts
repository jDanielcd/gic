import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AuthProvider } from "../auth/auth";

@Injectable()
export class PaymentsDbProvider {

  constructor (public afDB: AngularFireDatabase,
               public auth: AuthProvider) {}

  getPayments() {
    return this.afDB.list(this.auth.getUser() + '/payments').valueChanges();
  }

  getPayment(id) {
    return this.afDB.object(this.auth.getUser() + '/payments/' + id).valueChanges();
  }

  addPayment(payment) {
    payment.id = Date.now();
    this.afDB.database.ref(this.auth.getUser() + '/payments/' + payment.id).set(payment);
  }

  updatePayment(payment) {
    this.afDB.database.ref(this.auth.getUser() + '/payments/' + payment.id).set(payment);
  }

  deletePayment(payment) {
    this.afDB.database.ref(this.auth.getUser() + '/payments/' + payment.id).remove();
  }

}
