
import { Injectable } from '@angular/core';

/*
  Generated class for the TaxWithholdingsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TaxWithholdingsProvider {

  taxesWithholdings = [
    { id: 1, name: 'IRPF', description: 'Normal', value: 15 },
    { id: 2, name: 'IRPF', description: 'Reducido', value: 7 },
    { id: 3, name: 'IGIC', description: 'Excento', value: 0 }
  ];

  getTaxesWithholdings() {
    return this.taxesWithholdings;
  }

  getTaxWitholding(id) {
    return this.taxesWithholdings[id];
  }

}
