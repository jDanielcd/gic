import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AuthProvider } from "../auth/auth";

@Injectable()
export class MyCompanyDbProvider {

  constructor (public afDB: AngularFireDatabase,
               public auth: AuthProvider)
  {}

  getMyCompany() {
    return this.afDB.object(this.auth.getUser() + '/mycompany').valueChanges();
  }

  updateMyCompany(data){
    // data.id  = Date.now();
    return this.afDB.database.ref(this.auth.getUser() + '/mycompany').set(data);
  }

}
