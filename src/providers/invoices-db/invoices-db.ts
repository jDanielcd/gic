import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AuthProvider } from "../auth/auth";

@Injectable()
export class InvoicesDbProvider {

  constructor(public afDB: AngularFireDatabase,
              public auth: AuthProvider) {
  }

  getInvoices() {
    return this.afDB.list(this.auth.getUser() + '/invoices/').valueChanges();
  }

  getInvoice(id) {
    return this.afDB.object(this.auth.getUser() + '/invoices/' + id).valueChanges();
  }

  addInvoice(invoice) {
    invoice.id = Date.now();
    this.afDB.database.ref(this.auth.getUser() + '/invoices/' + invoice.id).set(invoice);
  }

  updateInvoice(invoice) {
    this.afDB.database.ref(this.auth.getUser() + '/invoices/' + invoice.id).set(invoice);
  }

  deleteInvoice(id) {
    this.afDB.database.ref(this.auth.getUser() + '/invoices/' + id).remove();
  }

}
