import { Injectable } from '@angular/core';

/*
  Generated class for the TaxesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TaxesProvider {

  taxes = [
    { id: 1, name: 'IGIC', description: 'Tipo cero', value: 0 },
    { id: 2, name: 'IGIC', description: 'Tipo reducido', value: 2.75 },
    { id: 3, name: 'IGIC', description: 'Tipo general', value: 7 },
    { id: 4, name: 'IGIC', description: 'Tipo incrementado', value: 13.5 },
    { id: 5, name: 'IGIC', description: 'Tipo especial', value: 20 },
  ];

  public getTaxes()
  {
    return this.taxes;
  }

  public getTax(id)
  {
    return this.taxes[id];
  }

}
